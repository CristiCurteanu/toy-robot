require_relative '../engine/robot_engine'
require 'ostruct'

RSpec.describe RobotEngine do
  context '.create' do
    it 'should return exception if initializer have no `x` position' do
      init = OpenStruct.new y: 3, facing: :north
      expect { described_class.create(init)}.to raise_error('Wrong coordinate')
    end

    it 'should return exception if initializer have no `y` position' do
      init = OpenStruct.new x: 3, facing: :north
      expect { described_class.create(init)}.to raise_error('Wrong coordinate')
    end

    it 'should return exception if initializer have no `facing` declared' do
      init = OpenStruct.new x: 3, y: 3
      expect { described_class.create(init)}.to raise_error('No facing defined')
    end

    it 'should return a new RobotEngine instance' do
      init = OpenStruct.new x: 1, y: 3, facing: :north
      instance = described_class.create(init)
      expect(instance).to be_kind_of(RobotEngine)
      expect(instance.x).to eql init.x
      expect(instance.y).to eql init.y
      expect(instance.facing).to eql init.facing
    end
  end

  context '#execute' do
    before(:each) do
      @commands = [
                    OpenStruct.new(action: :turn, value: :left),
                    OpenStruct.new(action: :move),
                    OpenStruct.new(action: :turn, value: :right),
                    OpenStruct.new(action: :report)
                  ]
    end

    it 'should execute commands' do
      init = OpenStruct.new x: 1, y: 3, facing: :north
      robot = described_class.create(init).execute(@commands)
      expect(robot.location).to eql "0,3,NORTH"
    end
  end
end