require_relative '../engine/commands'
require 'ostruct'

RSpec.describe Commands do
  before do
    @source = ["PLACE 0, 0, NORTH", "LEFT", "MOVE", "RIGHT", "REPORT"]
  end

  context '#origin' do
    it 'should return origin if source is valid' do
      expectance = OpenStruct.new x: 0, y: 0, facing: :north
      expect(described_class.new(@source).origin).to eql expectance
    end
  end

  context '#valid_commands' do
    it 'should return a list of commands if source is valid' do
      expectance = [
                    OpenStruct.new(action: :turn, value: :left),
                    OpenStruct.new(action: :move),
                    OpenStruct.new(action: :turn, value: :right),
                    OpenStruct.new(action: :report)
                  ]
      expect(described_class.new(@source).valid_commands).to eql expectance
    end
  end
end

