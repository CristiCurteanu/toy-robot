require_relative '../engine/commands'
require_relative '../engine/robot_engine'
require 'ostruct'

RSpec.describe 'RobotEngine behaviour on different inputs' do
  it 'should solve first sample' do
    commands = Commands.new File.open('spec/samples/first')
    robot = RobotEngine.create(commands.origin).execute(commands.valid_commands)
    expect(robot.location).to eql '0,1,NORTH'
  end

  it 'should solve second sample' do
    commands = Commands.new File.open('spec/samples/second')
    robot = RobotEngine.create(commands.origin).execute(commands.valid_commands)
    expect(robot.location).to eql '0,0,WEST'
  end

  it 'should solve third sample' do
    commands = Commands.new File.open('spec/samples/third')
    robot = RobotEngine.create(commands.origin).execute(commands.valid_commands)
    expect(robot.location).to eql '3,3,NORTH'
  end
end