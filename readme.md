# Introduction

Here is a `Toy Robot` application written in plain Ruby, which is a program that interprets and executes 'Robot DSL', and returns Robot location on a board.

# Usage

In order to run `Toy Robot` you need to give it instructions, in `instructions` file, and after to run ``` $ bin/robot ``` or to give a path to an instructions file from your system as a second argument, like: ``` $ bin/robot ~/path/to/the/source_file ```.

In the source file, first command will be `PLACE` command, followed by `x`, `y` location parameter, and `facing` paramter as well.