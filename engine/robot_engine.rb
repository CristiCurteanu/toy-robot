class RobotEngine
  ORIENTATIONS = %i(north east south west)

  attr_accessor :x, :y, :facing, :location

  def initialize(options = {})
    options.each { |k, v| instance_variable_set("@#{k}", v) }
  end

  def self.create(origin)
    self.new(x: origin.x, y: origin.y, facing: origin.facing) if valid_origin?(origin)
  end

  def execute(commands)
    commands.each do |command|
      if defined?(command.value)
        send(command.action, command.value)
      else
        send(command.action)
      end
    end
    self
  end

  def print_location
    puts @location
  end

  private

  def self.valid_origin?(origin)
    raise 'Wrong coordinate' unless origin.x && origin.y
    raise 'No facing defined' unless origin.facing
    true
  end

  def move
    range = [*0..4]
    case facing
    when :north then @y += 1 if range.include?(@y + 1)
    when :sourth then @y -= 1 if range.include?(@y - 1)
    when :east then @x += 1 if range.include?(@x + 1)
    when :west then @x -= 1 if range.include?(@x - 1)
    end
  end

  def turn(lookup)
    index = ORIENTATIONS.index(@facing)
    @facing = {
      left:  ORIENTATIONS[index - 1],
      right: ORIENTATIONS[index + 1] || ORIENTATIONS[0]
    }[lookup.downcase.to_sym]
  end

  def report
    @location ||= [@x, @y, @facing].map(&:to_s).join(',').upcase
  end
end