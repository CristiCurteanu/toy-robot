class Commands

  VALID_COMMANDS = %w(place move left right report)

  def initialize(source)
    @commands ||= parse source
  end

  def valid_commands
    @commands.reject { |el| defined?(el.facing) }
  end

  def origin
    @commands.find { |el| defined?(el.facing) }
  end

  private

  def parse(source)
    source.each_with_object([]) do |line, commands|
      if valid_command?(line)
        commands << convert_to_command(line.chomp)
      else
        raise("Invalid Syntax: '#{line}'")
      end
    end
  end

  def convert_to_command(line)
    if line['PLACE']
      command = line.split.map { |el| el.gsub(',', '')[/\d+/] ? el.to_i : el }
      OpenStruct.new x: command[1],
                     y: command[2],
                     facing: command.last.downcase.to_sym
    elsif line['LEFT'] || line['RIGHT']
      OpenStruct.new action: :turn, value: line.downcase.to_sym
    else
      OpenStruct.new action: line.downcase.to_sym
    end
  end

  def valid_command?(line)
    includes?(line) && valid_format?(line)
  end

  def includes?(line)
    VALID_COMMANDS.any? { |comm| line.downcase[comm] }
  end

  def valid_format?(line)
    {
      place: /PLACE\s\d{1},*.\d{1},*.[A-Z]{3,9}$/,
      move:  /MOVE|move/,
      left:  /LEFT|left/,
      right: /RIGHT|right/,
      report: /REPORT|report/
    }.any? { |k, v| line[v] }
  end
end